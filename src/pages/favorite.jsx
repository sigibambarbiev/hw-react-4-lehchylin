import React from 'react';
import {useSelector} from "react-redux";

import {Product} from "./products/product";
import s from "./products/productList.module.scss";

export function Favorite(props) {
    const {favorites, onButton, removeButtonStar, counterStarMinus} = props;
    const products = useSelector(store => store.productList);
    function PaintProductCard(product) {

        const {name, price, url, number, volume} = product;

        return (
            <>
                <Product
                    key={number}
                    name={name}
                    price={price}
                    url={url}
                    number={number}
                    volume={volume}
                    onButton={onButton}
                    removeButtonStar={removeButtonStar}
                    counterStarMinus={counterStarMinus}
                />
            </>
        )
    }

    return (
        <div>
            <div className={s.header}>
                <p className={s.title}>FAVORITES</p>
            </div>
            <div className={s.productList}>
                {products.map(product => favorites.includes(product.number) && PaintProductCard(product))}
            </div>
        </div>
    )
}
