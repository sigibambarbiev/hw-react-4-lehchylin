import React from 'react';
import {useSelector} from "react-redux";

import {Product} from "./products/product";
import s from "./products/productList.module.scss";


export function Cart(props) {

    const {cart, onButton, removeButtonCart, counterCartMinus} = props;
    const products = useSelector(store => store.productList);

    function PaintProductCard(product) {

        const {name, price, url, number, volume} = product;

        return (
            <>
                <Product
                    key={number}
                    name={name}
                    price={price}
                    url={url}
                    number={number}
                    volume={volume}
                    onButton={onButton}
                    removeButtonCart={removeButtonCart}
                    counterCartMinus={counterCartMinus}
                />
            </>
        )
    }

    return (
        <div>
            <div className={s.header}>
                <p className={s.title}>CART</p>
            </div>
            <div className={s.productList}>
                {products.map(product => cart.includes(product.number) && PaintProductCard(product))}
            </div>
        </div>
    )

}
