import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import PropTypes from 'prop-types';

import {Button} from "../../UI/Button/button";
import {Modal} from "../../UI/Modal/modal";
import {ModalTrue} from "../../actions";
import {ModalFalse} from "../../actions";

import star from "../../image/starWhite.png"
import starColor from "../../image/starColor1.png"
import s from "./product.module.scss"


export function Product(props) {

    const dispatch = useDispatch();
    const {
        name,
        price,
        url,
        number,
        volume,
        counterCartPlus,
        counterCartMinus,
        counterStarPlus,
        counterStarMinus,
        onButton,
        removeButtonCart,
        removeButtonStar,
    } = props;
    const [onStar, setOnStar] = useState(false);
    const [onCart, setOnCart] = useState(false);
    // const [openModal, setOpenModal] = useState(false);
    const [header, setHeader] = useState('');
    const [closeButton, setCloseButton] = useState(false);
    const [text, setText] = useState('');
    const [actions, setActions] = useState(<></>);

    const modalStatus = useSelector(store => store.product);
    const openModal = modalStatus[number];

    useEffect(() => {
        if (!!localStorage.getItem(`onStar${number}`)) {
            setOnStar(true)
        }
    }, [])

    useEffect(() => {
        if (!!localStorage.getItem(`onCart${number}`)) {
            setOnCart(true)
        }
    }, [])


    function handleClickDeleteProduct() {
        counterCartMinus(number);
        setOnCart(false);
        localStorage.removeItem(`onCart${number}`);
        closeModal()
    }

    function handleClickDeleteProductFav() {
        counterStarMinus(number);
        setOnStar(false);
        localStorage.removeItem(`onStar${number}`)
    }

    function handleClickStar() {

        if (!!onStar) {
            setOnStar(false);
            counterStarMinus(number);
            localStorage.removeItem(`onStar${number}`);
        } else {
            setOnStar(true);
            counterStarPlus(number);
            localStorage.setItem(`onStar${number}`, 'true');
        }
    }

    function handleClickCart() {

        if (!!onCart) {
            setOnCart(false);
            counterCartMinus(number);
            localStorage.removeItem(`onCart${number}`)
        } else {
            setOnCart(true);
            counterCartPlus(number);
            localStorage.setItem(`onCart${number}`, 'true')
        }
        closeModal()
    }

    function showModalFirst() {
        dispatch(ModalTrue(number));
        setHeader("Добавити обраний товар до кошика?");
        setText("Міністерство охорони здоров'я попереджає: помірне споживання алкоголю корисне для Вашого здоров'я!");
        setCloseButton(true);
        setActions(<>
            {Button(handleClickCart, 'Ok', '#5dc259')}
            {Button(closeModal, 'Cancel', '#dc6161')}
        </>)
    }

    function showModalCart() {
        dispatch(ModalTrue(number));
        setHeader("Видалити обраний товар з кошика?");
        setText("Товар буде видалено з кошика!");
        setCloseButton(true);
        setActions(<>
            {Button(handleClickDeleteProduct, 'Ok', '#5dc259')}
            {Button(closeModal, 'Cancel', '#dc6161')}
        </>)
    }

    function closeModal() {
        dispatch(ModalFalse(number))
    }


    return (
        <>
            <div className={s.product}>
                <div className={s.imageWrapp}>
                    <img className={s.image} src={url} alt={'productFoto'}/>
                    {!!removeButtonCart && <span className={s.modalCloseButton}><button className={s.closeButton} onClick={showModalCart}>X</button></span>}
                    {!!removeButtonStar && <span className={s.modalCloseButtonStar}><button className={s.closeButton} onClick={handleClickDeleteProductFav}><img className={s.Star} src={starColor} alt="star"/></button></span>}
                </div>
                <p className={s.name}>{name}</p>
                <p className={s.price}>{price}</p>
                <div className={s.wrappVolume}>
                    <p className={s.number}>Арт.{number}</p>
                    <p className={s.volume}>Об'єм: {volume}</p>
                </div>
                {!!onButton && <div className={s.buttonWrapper}>
                    {!onCart && Button(showModalFirst, 'ADD TO CART', '#94ef96')}
                    {onCart && Button(handleClickCart, 'REMOVE FROM CART', '#2ac72a')}
                    <button className={s.buttonStar} onClick={handleClickStar}>
                        <img className={s.Star} src={onStar ? starColor : star} alt="star"/>
                    </button>
                </div>}
            </div>
            {Modal(header, closeButton, text, actions, openModal, closeModal)}
        </>
    )
}


Product.propTypes = {
    name: PropTypes.string.isRequired,
    price: PropTypes.string,
    url: PropTypes.string,
    number: PropTypes.number.isRequired,
    volume: PropTypes.string,
}

Product.defaultProps = {
    price: '000.00',
    volume: '0.0 l',
}
