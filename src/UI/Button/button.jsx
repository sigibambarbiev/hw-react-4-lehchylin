
import PropTypes from 'prop-types';
import s from './Button.module.scss';

export function Button(onClick, text, backgroundColor) {

    return (
        <button className={s.btn} onClick={onClick} style={{backgroundColor: backgroundColor}}>
            {text}
        </button>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    backgroundColor: PropTypes.string
}

Button.defaultProps = {
    text: 'Button text',
    backgroundColor: '#FFFF'
}

