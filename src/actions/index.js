import {request} from "../tools/request";

const API_URL = './drinks.json';

export const GET_DRINKS = "GET_DRINKS";
export const GET_MODAL_STATUS = "GET_MODAL_STATUS";
export const CHANGE_MODAL_STATUS_TRUE = "CHANGE_MODAL_STATUS_TRUE";
export const CHANGE_MODAL_STATUS_FALSE = "CHANGE_MODAL_STATUS_FALSE";

export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";

export const ADD_TO_FAVORITES = "ADD_TO_FAVORITES";
export const REMOVE_FROM_FAVORITES = "REMOVE_FROM_FAVORITES";

const actionDrinks  = (response) => ({type:GET_DRINKS, payload:response});
const actionModalStatus = (modalStatus) => ({type:GET_MODAL_STATUS, payload:modalStatus});
const actionModalTrue = (number) => ({type: CHANGE_MODAL_STATUS_TRUE, payload: number});
const actionModalFalse = (number) => ({type: CHANGE_MODAL_STATUS_FALSE, payload: number});
// export const actionModalTrue = (number) => ({type: CHANGE_MODAL_STATUS_TRUE, payload: number});
// export const actionModalFalse = (number) => ({type: CHANGE_MODAL_STATUS_FALSE, payload: number});

export const ModalTrue = (number) => (dispatch) => {
    dispatch(actionModalTrue(number))};

export const ModalFalse = (number) => (dispatch) => {
    dispatch(actionModalFalse(number))};

export const fetchAllProducts = () => async (dispatch) => {
    const response = await request(API_URL)

    if (response) {
        dispatch(actionDrinks(response));

        const modalStatus = {};
        response.forEach(item => {
            for (const key in item) {
                if (key === "number") {
                    modalStatus[item[key]] = false;
                }
            }
        });
        dispatch(actionModalStatus(modalStatus));
    }
}