//заготовка на будущее
import * as actions from "../actions"

const defaultState = [];

export function cartReducer(state = defaultState, action) {
    switch (action.type) {
        case actions.ADD_TO_CART:
            return [...state, action.payload];
        case actions.REMOVE_FROM_CART:
            return [...state, action.payload];
        default:
            return state
    }

}